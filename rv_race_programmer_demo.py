import pandas as pd
import math
FieldSizeThreshold=10


def create_input_for_race_program_module():
    race_list = {
        "3Y SWP": {
            "field_size": 20,
            "prize_money": 100000,
            "acceptances": 17,
            "race_type": "3Y SWP",
            "meet_venue": "Flemington",
            "meet_date": "2016-11-01",
            "nominations": 20,
            "turnover": 15248970.79,
            "meet_title": "Melbourne Cup"
        },
        "LEXUS HYBRID": {
            "field_size": 16,
            "prize_money": 120000,
            "acceptances": 25,
            "race_type": "LEXUS HYBRID",
            "meet_venue": "Flemington",
            "meet_date": "2016-11-01",
            "nominations": 31,
            "turnover": 17783848.28,
            "meet_title": "Melbourne Cup"
        },
        "MSS SECURITY": {
            "field_size": 20,
            "prize_money": 120000,
            "acceptances": 19,
            "race_type": "MSS SECURITY",
            "meet_venue": "Flemington",
            "meet_date": "2016-11-01",
            "nominations": 36,
            "turnover": 20889323.95,
            "meet_title": "Melbourne Cup"
        },
        "4&5Y 0 - 90": {
            "field_size": 16,
            "prize_money": 100000,
            "acceptances": 19,
            "race_type": "4&5Y 0 - 90",
            "meet_venue": "Flemington",
            "meet_date": "2014-11-04",
            "nominations": 39,
            "turnover": 13154791.66,
            "meet_title": "Melbourne Cup"
        },
        "4&5Y BM90": {
            "field_size": 16,
            "prize_money": 100000,
            "acceptances": 14,
            "race_type": "4&5Y BM90",
            "meet_venue": "Flemington",
            "meet_date": "2016-11-01",
            "nominations": 28,
            "turnover": 12170787.06,
            "meet_title": "Melbourne Cup"
        },
        "OTTAWA": {
            "field_size": 16,
            "prize_money": 150000,
            "acceptances": 29,
            "race_type": "OTTAWA",
            "meet_venue": "Flemington",
            "meet_date": "2016-11-01",
            "nominations": 31,
            "turnover": 10280541.85,
            "meet_title": "Melbourne Cup"
        },
        "MAYBE MAHAL": {
            "field_size": 16,
            "prize_money": 150000,
            "acceptances": 18,
            "race_type": "MAYBE MAHAL",
            "meet_venue": "Flemington",
            "meet_date": "2016-11-01",
            "nominations": 34,
            "turnover": 23989696.18,
            "meet_title": "Melbourne Cup"
        },
        "CUP DAY PLATE": {
            "field_size": 18,
            "prize_money": 120000,
            "acceptances": 20,
            "race_type": "CUP DAY PLATE",
            "meet_venue": "Flemington",
            "meet_date": "2016-11-01",
            "nominations": 42,
            "turnover": 17648576.55,
            "meet_title": "Melbourne Cup"
        },
        "MELB CUP": {
            "field_size": 24,
            "prize_money": 4850000,
            "acceptances": 27,
            "race_type": "MELB CUP",
            "meet_venue": "Flemington",
            "meet_date": "2016-11-01",
            "nominations": 123,
            "turnover": 247124662.4,
            "meet_title": "Melbourne Cup"
        },
        "0 - 96": {
            "field_size": 18,
            "prize_money": 100000,
            "acceptances": 21,
            "race_type": "0 - 96",
            "meet_venue": "Flemington",
            "meet_date": "2014-11-04",
            "nominations": 36,
            "turnover": 10963880.51,
            "meet_title": "Melbourne Cup"
        },
        "BM96": {
            "field_size": 18,
            "prize_money": 100000,
            "acceptances": 24,
            "race_type": "BM96",
            "meet_venue": "Flemington",
            "meet_date": "2016-11-01",
            "nominations": 35,
            "turnover": 12557352.07,
            "meet_title": "Melbourne Cup"
        },
        "MARES  HCP": {
            "field_size": 16,
            "prize_money": 100000,
            "acceptances": 14,
            "race_type": "MARES  HCP",
            "meet_venue": "Flemington",
            "meet_date": "2016-11-01",
            "nominations": 31,
            "turnover": 10612820.38,
            "meet_title": "Melbourne Cup"
        }
    }


    return race_list

def obtain_race_program(meet_capacity,race_list):
    original_total_field_size = meet_capacity
    race = []
    fc_wagering = []
    fc_horse_population = []
    race_field_size = []
    for race_name in race_list:
        race.append(race_name)
        fc_wagering.append(race_list[race_name]['turnover'])
        fc_horse_population.append(race_list[race_name]['acceptances'])
        race_field_size.append(race_list[race_name]['field_size'])

    sorted_race = [x for _, x in sorted(zip(fc_wagering, race), reverse=1)]
    sorted_fc_horse_population = [x for _, x in sorted(
        zip(fc_wagering, fc_horse_population), reverse=1)]

    # Wendy change
    sorted_field_size = [x for _, x in sorted(zip(fc_wagering, race_field_size), reverse=1)]
    # race_field_size = input_program['race_field_size']
    flag_final = False
    num_default = [0] * len(race)
    prog_race_number = pd.Series(num_default, index=sorted_race)
    prog_field_size = pd.Series(num_default, index=sorted_race)
    temp_prog_field_size = pd.Series(num_default, index=sorted_race)
    total_field_size = original_total_field_size
    while total_field_size:
        if flag_final:
            break
        for r, fc_hp, field_size in zip(sorted_race, sorted_fc_horse_population, sorted_field_size):
            if total_field_size <= 0:
                flag_final = True
                break

            if fc_hp < total_field_size:
                num_race = int(math.floor(fc_hp / field_size))
                if num_race < 1:
                    num_race = 1
                    if fc_hp > field_size:
                        temp_prog_field_size[r] = num_race * field_size
                        prog_race_number[r] = num_race
                    else:
                        temp_prog_field_size[r] = num_race * fc_hp
                        prog_race_number[r] = num_race
                else:
                    if fc_hp > field_size:
                        temp_prog_field_size[r] = num_race * field_size
                        prog_race_number[r] = num_race
                    else:
                        temp_prog_field_size[r] = num_race * fc_hp
                        prog_race_number[r] = num_race

            else:
                num_race = int(math.ceil(fc_hp / field_size))
                temp_prog_field_size[r] = num_race * fc_hp
                prog_race_number[r] = num_race
            prog_field_size = temp_prog_field_size
            total_field_size -= prog_field_size[r]

    temp_prog_field_size_cumsum = temp_prog_field_size.cumsum()
    prog_race_number[temp_prog_field_size_cumsum > original_total_field_size] = 0
    prog_field_size[temp_prog_field_size_cumsum > original_total_field_size] = 0
    return prog_field_size,prog_race_number

#---------------------------------------------------------------------------#
if __name__ == '__main__':
    race_list=create_input_for_race_program_module()
    meet_capacity=180
    race_number,race_field_size=obtain_race_program(meet_capacity,race_list)


    print('Selected Race Field size Info')
    print(race_number)
    print('Selected Race Field size Info')
    print(race_field_size)

