import numpy as np
import random
import pandas as pd
import pickle
from fuzzywuzzy import process
from datetime import datetime, timedelta
fixed_fixture_path = './data/fixed_meeting_final.xlsx'
rv_fixture_2016_path='./data/rv_fixture_2016.csv'



def date_adjustment(date):
    #print(date)
    adjust_type='Forward'
    if adjust_type == 'Forward':
        try:
            adjusted_date = date + timedelta(days=366)
        except ValueError:
            return None

    year = date.year
    adjusted_year = adjusted_date.year

    if adjusted_year - year > 1:
        return None

    week_day = date.weekday()
    adjusted_week_day = adjusted_date.weekday()

    if week_day > adjusted_week_day:
        shift_num = (week_day - adjusted_week_day)
    else:
        shift_num = (week_day + 7 - adjusted_week_day)

    try:
        adjusted_date = adjusted_date + timedelta(days=shift_num)
    except ValueError:
        return None

    return adjusted_date


def distribute_num_using_prob_dist(norm_pred_series,act_number):
    random.seed(0)
    norm_pred_series_act_number = (norm_pred_series * act_number)
    norm_pred_series_act_number_ceil= norm_pred_series_act_number.apply(np.ceil)
    pred_number_dist = norm_pred_series_act_number_ceil.sum()

    ind=0
    while True:
        if act_number == pred_number_dist:
            return  norm_pred_series_act_number_ceil
        norm_pred_series_act_number_ceil[ind]=norm_pred_series_act_number_ceil[ind]-1
        pred_number_dist=norm_pred_series_act_number_ceil.sum()
        # print(pred_number_dist)
        ind+=1

    return  norm_pred_series_act_number_ceil

def map_venue_name_from_created_meet_title(venue_title):

    if '-MeetType-Metro-' in venue_title:
        venue_name = venue_title.split('-MeetType-Metro-')[0]
        venue_name = venue_name.split('Venue-')[1]

    else:
        venue_name = venue_title.split('-MeetType-Country-')[0]
        venue_name = venue_name.split('Venue-')[1]

    return venue_name



def process_day_prefence_input(day_pref_input):
    months = list(range(1, 12 + 1))
    day_preference_req_all_year = []
    for month in months:
        day_preference_req_month = []
        for day_pref_ in day_pref_input:
            if (day_pref_['month'] == month):
                day_preference_req_month.append(day_pref_)
        if len(day_preference_req_month)==0:
            day_preference_req_month=['Null']

        # day_preference_req_month = day_preference_req_month * 8
        day_preference_req_all_year.append(day_preference_req_month)
        #print(len(day_preference_req_month))

    return day_preference_req_all_year

def convert_fixture_list_to_dataframe(fixture_whole_year):
    fixture_dic = {}
    venue = []
    date = []
    turnover = []
    meet_Type = []
    day_night = []
    tab_status = []
    for fixture in fixture_whole_year:
        for meet in fixture:
            venue.append(meet['venue'])
            date.append(meet['date'])
            turnover.append(meet['turnover'])
            meet_Type.append(meet['meet_type'])
            day_night.append(meet['day_night'])
            tab_status.append(meet['tab_status'])

    fixture_dic['Venue'] = venue
    fixture_dic['Date'] = date
    fixture_dic['Meet_Type'] = meet_Type
    fixture_dic['Day_Night'] = day_night
    fixture_dic['Tab_Status'] = tab_status
    fixture_dic['Pred_Turnover'] = turnover
    fixture_df = pd.DataFrame(fixture_dic)
    fixture_df['Date'] = pd.to_datetime(fixture_df['Date'])
    return fixture_df


def find_meeting_day_type(df):
    df['Day_Type'] = pd.to_datetime(df['Date'])
    df['Day_Type'] = df['Day_Type'].dt.weekday_name

    conditions = [df['Day_Type'] == 'Satuday', df['Day_Type'] == 'Sunday', df['Day_Night'] == 'N']
    choices = ['Saturday', 'Sunday', 'Night']
    df['Day_Type'] = np.select(conditions, choices, default='MidWeek')
    return df


def append_series(input_series):
    ## append the series ####
    for i in range(len(input_series)):
        if i == 0:
            temp_comb_input_series = input_series[i]
            comb_input_series = temp_comb_input_series
            # comb_input_series = temp_comb_input_series[temp_comb_input_series > 0]
            # comb_input_series = temp_comb_input_series[temp_comb_input_series > 0]
        else:
            temp_comb_input_series = input_series[i]
            #comb_input_series = temp_comb_input_series
            # comb_input_series = temp_comb_input_series[temp_comb_input_series > 0]
            # temp_comb_input_series = pd.Series(temp_comb_input_series[temp_comb_input_series > 0])
            comb_input_series = pd.concat([comb_input_series, temp_comb_input_series])

    # comb_input_series = comb_input_series.groupby(comb_input_series.index).sum()
    return  comb_input_series


def map_venue_name_from_created_meet_title(venue_title):
    if '-Metro-' in venue_title:
        venue_name=venue_title.split('-Metro-')[0]
    else:
        venue_name = venue_title.split('-Country-')[0]

    return venue_name



def process_auto_generated_fixture(auto_fixture_path):
    auto_meeting = pd.read_csv(auto_fixture_path)
    non_fixed_meeting_dic = {}
    title_temp=auto_meeting['Venue'].values
    non_fixed_meeting_dic['Venue'] = list(map(map_venue_name_from_created_meet_title,title_temp))
    non_fixed_meeting_dic['Date'] = auto_meeting['Date'].values
    non_fixed_meeting_dic['Meet_Type'] = auto_meeting['Meet_Type'].values
    non_fixed_meeting_dic['Tab_Status'] = auto_meeting['Tab_Status'].values
    non_fixed_meeting_dic['Day_Night'] = auto_meeting['Day_Night'].values
    non_fixed_meeting_dic['Title'] = auto_meeting['Venue'].values
    non_fixed_meeting_dic['Pred_Turnover'] = auto_meeting['Pred_Turnover'].values
    non_fixed_meeting_dataframe = pd.DataFrame(non_fixed_meeting_dic)
    return non_fixed_meeting_dataframe

def process_fixed_fixture(fixed_fixture_path):
    fixed_meeting = pd.read_excel(fixed_fixture_path)
    fixed_meeting=fixed_meeting.dropna()
    meeting_all_2017 = fixed_meeting[fixed_meeting['MeetingDate'].dt.year == 2017]
    meeting_all_2018=fixed_meeting[fixed_meeting['MeetingDate'].dt.year==2018]
    meeting_all_2018['MeetingDate']= meeting_all_2018['MeetingDate'].apply(lambda x: x -pd.DateOffset(years=1))
    fixed_meeting_edited=pd.concat([meeting_all_2018,meeting_all_2017])
    meet_title=fixed_meeting_edited['Meeting Title'].values
    venue=fixed_meeting_edited['Venue'].values
    date=fixed_meeting_edited['MeetingDate'].values
    tab_status=fixed_meeting_edited['TAB/Non TAB'].values
    meet_type=fixed_meeting_edited['MeetingType'].values
    day_night=fixed_meeting_edited['Day/Night'].values

    tab_status=list(map(lambda x:'Y' if x=='TAB' else 'N',tab_status))
    day_night =list(map(lambda x:'Y' if x=='Day' else 'N',day_night))

    fixed_meeting_dic={}
    fixed_meeting_dic['Venue']=venue
    fixed_meeting_dic['Title']=meet_title
    fixed_meeting_dic['Date']=date
    fixed_meeting_dic['Meet_Type']=meet_type
    fixed_meeting_dic['Tab_Status']=tab_status
    fixed_meeting_dic['Day_Night']=day_night
    fixed_meeting_dic['Pred_Turnover'] = list(np.zeros(len(venue)))
    fixed_meeting_dataframe=pd.DataFrame(fixed_meeting_dic)

    return fixed_meeting_dataframe

def create_fixed_block_preference():
    df=process_fixed_fixture(fixed_fixture_path)
    df['Month']=pd.to_datetime(df['Date']).dt.month
    df['Day'] = pd.to_datetime(df['Date']).dt.day
    df_gp=df.groupby(['Venue','Month'])['Date']
    fixed_block=[]
    for name, group in df_gp:
        dates=list(group.values)
        days_string=[str(d) for d in dates]
        days = list(map(lambda x: int(x.split('-')[1]), days_string))
        # days=list(map(lambda x:int(x.split('-')[2]),dates))
        block_fixed_calendar={}
        block_fixed_calendar['venue']=name[0]
        block_fixed_calendar['month']=name[1]
        block_fixed_calendar['days']=days
        block_fixed_calendar['req_type']='block'
        fixed_block.append(block_fixed_calendar)
    return fixed_block


def forcast_turnover_for_fixed_meeting(fixed_meeting_dateframe):
    # input_venue_name = 'Werribee'
    # input_month = '01'
    # input_day_night = 'D'
    filename = './model/model.pkl'
    model=pickle.load(open(filename, 'rb'))
    filename = './model/dm_venue.pkl'
    dm_venue=pickle.load(open(filename, 'rb'))
    filename = './model/dm_month.pkl'
    dm_month=pickle.load(open(filename, 'rb'))


    input_venue_list=fixed_meeting_dateframe['Venue'].values
    input_month_list_=fixed_meeting_dateframe['Date'].dt.month
    input_month_list=list(map(lambda x:str(x) if x>9 else '0'+str(x),input_month_list_))
    meet_type_list=fixed_meeting_dateframe['Meet_Type'].values
    day_night_list=fixed_meeting_dateframe['Day_Night'].values
    tab_status_list=fixed_meeting_dateframe['Tab_Status'].values
    pred_turnover_all=[]


    for input_venue_name,input_month,meet_type,day_night,tab_status in zip(input_venue_list,input_month_list,meet_type_list,day_night_list,tab_status_list):

        #######Create dictonary for mapping input into one-hot encoded vector #############
        venue_name = dm_venue.columns
        venue_index = np.zeros([len(venue_name), 1])[0]
        temp_venue_name = list(venue_name)

        # venue_index = np.zeros(len(venue_name))
        venue_series = pd.Series(list(venue_index), index=venue_name)

        month_name = dm_month.columns
        month_index = np.zeros([len(month_name), 1])[0]
        # month_index = np.zeros(len(month_name))
        month_series = pd.Series(list(month_index), index=month_name)
        # print(input_venue_name)
        # print('Venue Number=',len(venue_series))
        #######encode input into one hot vector#############################################
        if input_venue_name not in venue_name:
            close_match=process.extractOne(input_venue_name, list(venue_name))[0]
            input_venue_name=close_match
            #print('Closest Venue Name',input_venue_name)

        venue_series[input_venue_name] = 1
        month_series[input_month] = 1

        ###meet property####################################################################

        feature_meet_property=[]
        if meet_type=='Country':
            meet_type_flag=0
        else:
            meet_type_flag=1

        if day_night=='Day':
            dn_flag=1
        else:
            dn_flag=0

        if tab_status=='TAB':
            tb_flag=1
        else:
            tb_flag=0

        feature_meet_property.append(meet_type_flag)
        feature_meet_property.append(dn_flag)
        feature_meet_property.append(tb_flag)
        # feature_meet_property=np.reshape(np.asarray(feature_meet_property),[len(feature_meet_property),1])
        feature_meet_property=np.asarray(feature_meet_property)
        feature_venue = venue_series.values
        feature_month = month_series.values

        # print(feature_month)
        comb_feature = np.concatenate((feature_venue, feature_month,feature_meet_property))
        comb_feature = np.reshape(comb_feature, [len(comb_feature), 1])
        comb_feature = np.transpose(comb_feature)
        #print(np.shape(feature_venue),np.shape(feature_month),np.shape(comb_feature))
        pred_turnover = np.exp(model.predict(comb_feature))[0]
        #print(pred_turnover)
        # print(input_venue_name,pred_turnover)
        pred_turnover_all.append(pred_turnover)

    return pred_turnover_all





