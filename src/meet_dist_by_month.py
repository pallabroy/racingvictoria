import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from sklearn.ensemble import ExtraTreesRegressor
from sklearn.metrics import mean_squared_error
from math import sqrt
from datetime import date, timedelta
from src.rv_util import distribute_num_using_prob_dist
import numpy as np
NUM_TREES=200
TURNOVER_DATA= './data/rv_turnover_per_meeting.csv'
# TURNOVER_DATA= '../data/rv_turnover_per_meeting.csv'
NUM_FORCAST_MONTH=12
lag=12

def find_day(full_date):
    splited_date=full_date.split('-')
    return (splited_date[0]+'-'+splited_date[1])

def find_unique_year(year_month_date):
    meet_year=set()
    for meet_date in year_month_date:
        splited_date = meet_date.split('-')
        meet_year.add(splited_date[0])
    return list(meet_year)

def find_meet_number_by_year(turnover_data):
    year_month_date = list(turnover_data['Meet_Date'])
    meet_years = find_unique_year(year_month_date)
    meet_num__by_year={}
    for meet_year in meet_years:
        meet_num=len(turnover_data[turnover_data['Meet_Date'].str.contains(meet_year)])
        meet_num__by_year[meet_year]=meet_num
    return meet_num__by_year

def compute_norm_meet_number(meet_num__by_year, meet_nums, year_months):
    norm_meet_number=[]
    for year_month, meet_num in zip(year_months, meet_nums):
        year_=year_month.split('-')[0]
        norm_meet_num= (meet_num / meet_num__by_year[year_])*100
        norm_meet_number.append(norm_meet_num)
    return  norm_meet_number

def create_meeting_time_series():
    turnover_data = pd.read_csv(TURNOVER_DATA)
    meet_num_by_year=find_meet_number_by_year(turnover_data)
    turnover_data_gp_date = turnover_data.groupby('Meet_Date')['Meet_Code'].count()
    meet_num = list(turnover_data_gp_date.values)
    year_month_date = list(turnover_data_gp_date.index)
    year_months = list(map(find_day, year_month_date))
    ts_data_dic = {}
    ts_data_dic['Month'] = year_months
    ts_data_dic['Num_Meeting'] = meet_num
    ts_data_frame = pd.DataFrame(ts_data_dic)
    ts_data_frame.reset_index(inplace=True)
    # ts_data_frame['Month'] = pd.to_datetime(ts_data_frame['Month'])
    ts_data_frame = ts_data_frame.set_index('Month')
    month_meet_num_ts = ts_data_frame.groupby('Month')['Num_Meeting'].sum()
    norm_meet_num = compute_norm_meet_number(meet_num_by_year,list(month_meet_num_ts.values), list(month_meet_num_ts.index))
    norm_month_meet_num_ts=pd.Series(norm_meet_num,index=month_meet_num_ts.index)
    return norm_month_meet_num_ts,month_meet_num_ts,meet_num_by_year


def train_forecasting_method(month_meet_num_ts,lag):
    shifted = month_meet_num_ts.shift(lag)
    differenced = month_meet_num_ts.diff(lag)
    differenced = differenced[lag:]
    shifted = shifted[lag:]
    dataframe = pd.DataFrame()
    for i in range(lag, 0, -1):
        dataframe['t-' + str(i)] = differenced.shift(i)
    dataframe['t'] = differenced.values
    dataframe = dataframe[lag+1:]
    shifted=shifted[lag+1:]
    array = dataframe.values
    # split into input and output
    X = array[:, 0:-1]
    y = array[:, -1]
    # # fit random forest model
    model = ExtraTreesRegressor(n_estimators=NUM_TREES,max_depth=10,random_state=0)
    model.fit(X, y)
    y_pred_diff=model.predict(X)
    y_pred=y_pred_diff+shifted.values
    y=y+shifted.values
    rms = sqrt(mean_squared_error(y, y_pred))
    act_meet_num_dic={}
    act_meet_num_dic['Meet_Num']=y
    act_meet_dist=pd.Series(y,index=shifted.index)
    pred_meet_dist= pd.Series(y_pred, index=shifted.index)

    # plt.figure()
    # plt.plot(act_meet_dist.tail(24))
    # plt.plot(pred_meet_dist.tail(24))
    # plt.title(rms)
    # plt.ylabel('Percentage of Number of Meeting')
    # plt.legend(['Original Dist of Meeting','Estimated Dist of Meeting'])
    # plt.show()
    return model,act_meet_dist,pred_meet_dist

def forecast_meeting_distribution(model,month_meet_num_ts_train,end_date,lag):
    month_meet_num_ts_train_diff = month_meet_num_ts_train.diff(lag)
    meet_prct_diff = month_meet_num_ts_train_diff.tail(lag)
    meet_prct = month_meet_num_ts_train.tail(lag)
    x_diff_list = list(meet_prct_diff)
    x = list(month_meet_num_ts_train.tail(lag))
    start_date_str = meet_prct.index[-1]
    year_month = start_date_str.split('-')
    start_date = date(int(year_month[0]), int(year_month[1]), 1)
    delta = end_date - start_date
    date_list = []
    num_forcast_month = 0
    for i in range(31, delta.days + 31, 31):
        temp_date = str(start_date + timedelta(days=i))
        dates_arr = temp_date.split('-')
        date_str = dates_arr[0] + '-' + dates_arr[1]
        date_list.append(date_str)
        num_forcast_month += 1
    pred_meet_prct_list = []
    for i in range(num_forcast_month):
        x_diff_array = np.reshape(x_diff_list, [1, -1])
        pred_x_diff = model.predict(x_diff_array)[0]
        pred_x = pred_x_diff + x[0]
        pred_meet_prct_list.append(pred_x)
        x.pop(0)
        x.append(pred_x)
        x_diff_list.pop(0)
        x_diff_list.append(pred_x_diff)
    pred_meet_dist_series = pd.Series(pred_meet_prct_list, index=date_list)
    return  pred_meet_dist_series


def distribute_meet_to_months_by_year(total_meet_num):
    end_date = date(2017, 12, 1)  # end date
    month_meet_num_ts_norm, month_meet_num_ts, meet_num_by_year = create_meeting_time_series()
    month_meet_num_ts_train = month_meet_num_ts_norm['2006-01':'2016-12']
    month_meet_num_ts_test = month_meet_num_ts_norm['2017-01':'2017-12']
    model, act_meet_dist_train, pred_meet_dist_train = train_forecasting_method(month_meet_num_ts_train, lag)
    pred_meet_dist_test = forecast_meeting_distribution(model, month_meet_num_ts_train, end_date, lag)
    pred_meet_dist_test = (pred_meet_dist_test.tail(NUM_FORCAST_MONTH) / sum(
        pred_meet_dist_test.tail(NUM_FORCAST_MONTH))) * 100
    all_pred_meet_dist = pd.concat([pred_meet_dist_train, pred_meet_dist_test], axis=0)
    act_meet_number = month_meet_num_ts_test
    pred_meet_number = all_pred_meet_dist.tail(len(month_meet_num_ts_test))
    #######Visulation forecast and actual meeting dist over month
    # rms = sqrt(mean_squared_error(act_meet_number.tail(NUM_FORCAST_MONTH), pred_meet_number.tail(NUM_FORCAST_MONTH)))
    # plt.plot(act_meet_number.tail(NUM_FORCAST_MONTH))
    # plt.plot(pred_meet_number.tail(NUM_FORCAST_MONTH))
    # title_str = 'RMS=' + str(rms)
    # plt.title(title_str)
    # plt.ylabel('Percentage of Number of Meeting')
    # plt.legend(['Original Dist of Meeting', 'Estimated Dist of Meeting'])
    # plt.show()

    pred_meet_number = pred_meet_number / 100
    meet_number_month = distribute_num_using_prob_dist(pred_meet_number, total_meet_num)

    #####Visulation of the distribution of the given number of meeting########################################
    # plt.plot(meet_number_month)
    # plt.ylabel('Number of Meeting')
    # plt.title('Total Number of Meeting=' + str(total_meet_num))
    # plt.show()
    return meet_number_month


















