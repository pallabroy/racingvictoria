import numpy as np
import math

class MeetingSchedular(object):

    def __init__(self,MEETING,VENUES,DATES,TURNOVER):
        self.MEETING=MEETING
        self.VENUES =VENUES
        self.DATES =DATES
        self.NUM_DATES=len(DATES)
        self.TURNOVER = TURNOVER

    def check_constrain(self,cons_venue, cons_day, selected_venue, selected_day, selected_col, ind_max_rev, day_, venue_):
        ## count the frequency of the selected venue
        count_venue_ = selected_venue.count(venue_)
        ## count the frequency of the selected day
        count_day_ = selected_day.count(day_)
        ## check wheather the selected venue meet the constrain or not
        if ((count_venue_ >= cons_venue[venue_]) or (cons_venue[venue_] == 0)):
            return True
        ## check wheather the selected day meet the constrain or not
        elif ((count_day_ >= cons_day[day_]) or (cons_day[day_] == 0)):
            return True
        ## slected unique venue and day combination for each meeting
        elif len(selected_col)>0:
            if(ind_max_rev in selected_col):
                return True
            else:
                return False
        else:
            return False

    def find_day_venue_ind(self,col_ind):
        v_ind = math.ceil((col_ind + 1) / self.NUM_DATES)
        d_ind = (col_ind + 1) - ((v_ind - 1) * self.NUM_DATES)
        return self.VENUES[v_ind - 1], self.DATES[d_ind - 1]

    def map_venue_day_max_rev_col(self,row_rev,row_rev_sorted,count):
        ind_max_rev = row_rev.index(row_rev_sorted[count])
        ## find venue and date for the max turnover
        venue_, day_ = self.find_day_venue_ind(ind_max_rev)
        return venue_,day_,ind_max_rev

    def do_bf_meeting_assignment(self,cons_venue, cons_day,turnover_matrix):
        # print(self.TURNOVER)
        n_row,n_col=np.shape(turnover_matrix)
        selected_venue=[]
        selected_day=[]
        selected_col=[]
        row_ind=[]
        col_ind=[]
        fixture_list=[]

        for i,r in enumerate(range(n_row)):
            info_dic={}
            ## turnover  for each row/meeting
            count=0
            row_rev=list(turnover_matrix[r, :])
            ## sort the turnover in descending order
            row_rev_sorted=list(np.sort(row_rev)[::-1])
            ## find venue and date for the max turnover
            venue_,day_,ind_max_rev=self.map_venue_day_max_rev_col(row_rev, row_rev_sorted, count)
            ## check all the constrain
            constrain_flag=self.check_constrain(cons_venue, cons_day, selected_venue, selected_day,
                                       selected_col, ind_max_rev, day_, venue_)
            while True:
                if not constrain_flag:
                    selected_col.append(ind_max_rev)
                    selected_venue.append(venue_)
                    selected_day.append(day_)
                    col_ind.append(ind_max_rev)
                    row_ind.append(i)
                    # print(venue_,day_,self.TURNOVER[i,ind_max_rev])
                    info_dic['venue']=venue_
                    info_dic['date']=day_
                    info_dic['turnover']=self.TURNOVER[i,ind_max_rev]
                    venue_split = venue_.split('-')
                    info_dic['meet_type']=venue_split[1]
                    info_dic['day_night'] = venue_split[-1]
                    info_dic['tab_status'] = venue_split[3]

                    fixture_list.append(info_dic)

                    break
                else:
                    if count == n_col-1:
                        break
                    row_rev[ind_max_rev]=-1
                    count+=1
                    #print(count,n_col)

                    ind_max_rev = row_rev.index(row_rev_sorted[count])
                    venue_, day_ = self.find_day_venue_ind(ind_max_rev)
                    constrain_flag = self.check_constrain(cons_venue, cons_day, selected_venue, selected_day, selected_col,
                                             ind_max_rev, day_, venue_)


        return fixture_list





