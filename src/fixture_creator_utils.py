import numpy as np
import numpy.matlib
from src.meeting_schedular import MeetingSchedular
from calendar import monthrange
import random
from src.meet_dist_by_month import distribute_meet_to_months_by_year
from src.meet_dist_by_venue_meet_type import distribute_meet_to_venue_by_month_meet_type_combined
from src.rv_util import process_day_prefence_input,convert_fixture_list_to_dataframe,forcast_turnover_for_fixed_meeting
import pandas as pd
from src.rv_util import distribute_num_using_prob_dist,process_auto_generated_fixture,process_fixed_fixture
from matplotlib import pyplot as plt
from datetime import datetime, timedelta
import datetime
from src.rv_util import map_venue_name_from_created_meet_title


# auto_fixture_path = '../data/auto_generated_fixture_2017.csv'
# fixed_fixture_path = '../data/fixed_meeting_final.xlsx'
# TURNOVER_DATA= '../data/rv_turnover_per_meeting.csv'

auto_fixture_path = './data/auto_generated_fixture_2017.csv'
fixed_fixture_path = './data/fixed_meeting_final.xlsx'
TURNOVER_DATA= './data/rv_turnover_per_meeting.csv'


def create_cons_venue_day_preference(meet_dist_filt_venue,day_preference_req,num_days):

    venue_name = list(meet_dist_filt_venue.index)
    cons_venue = {}
    for v in venue_name:
        cons_venue[v] = meet_dist_filt_venue[v]

    for ob in day_preference_req:
        if ob['venue'] in venue_name:
            if ob['req_type']=='block':
                num_block_days=len(ob['days'])
                available_day=num_days-num_block_days
                if available_day<cons_venue[ob['venue']]:
                    extra_meet=cons_venue[ob['venue']]-available_day
                    cons_venue[ob['venue']] = available_day
                    indx=venue_name.index(ob['venue'])
                    next_venue=venue_name[indx+1]
                    cons_venue[next_venue]=cons_venue[next_venue]+extra_meet

    return cons_venue



def create_cons_date(num_days,input_year,input_month,number_of_meet):
    meet_spread_per_day_current_year=find_day_spread_constrain_prev_calendar(input_year,input_month,number_of_meet)
    meet_spread_per_day_current_year[meet_spread_per_day_current_year==0]=1
    cons_date={}
    for i in range(1,num_days+1,1):
        dt = datetime.datetime(int(input_year), int(input_month), i)
        try:
            cons_date[input_year+'-'+input_month+'-'+str(i)]=meet_spread_per_day_current_year[dt]
        except Exception:
            # if using the 365 day offset  date preference
            cons_date[input_year+'-'+input_month+'-'+str(i)]=0

    return cons_date


def create_day_importance_vector(input_year,input_month,venue_name):
    fill_nan=.1
    turnover_data = pd.read_csv(TURNOVER_DATA)
    turnover_data_no_nan = turnover_data.dropna()
    turnover_data_no_nan['Year'] = pd.to_datetime(turnover_data_no_nan['Meet_Date']).dt.year
    turnover_data_no_nan['Month']=pd.to_datetime(turnover_data_no_nan['Meet_Date']).dt.month
    turnover_data_no_nan['Weekday'] = pd.to_datetime(turnover_data_no_nan['Meet_Date']).dt.weekday
    num_days = monthrange(int(input_year), int(input_month))[1]
    day_importance = {}
    for v_n in venue_name:
        v_n_temp=map_venue_name_from_created_meet_title(v_n)
        turnover_data_no_nan_filt=turnover_data_no_nan[(turnover_data_no_nan['Year']==int(input_year)-1) & (turnover_data_no_nan['Month']==int(input_month)) & (turnover_data_no_nan['Venue']==v_n_temp) ]
        turnover_data_no_nan_filter_gp_weekday=turnover_data_no_nan_filt.groupby(['Weekday'])['Meet_Code'].count()
        week_day_importance_vector=pd.Series([0]*7,index=list(range(0,7)))
        for week_day_ind in turnover_data_no_nan_filter_gp_weekday.index:
            week_day_importance_vector[week_day_ind]=turnover_data_no_nan_filter_gp_weekday[week_day_ind]
        prob_week_day_importance_vector=week_day_importance_vector/sum(week_day_importance_vector)
        prob_week_day_importance_vector=prob_week_day_importance_vector+.1
        imp_vect=[]
        for d in range(0,num_days):
            input_date=datetime.date(int(input_year), int(input_month), d+1)
            if np.isnan(prob_week_day_importance_vector[input_date.weekday()]):
                print(v_n)
                imp_vect.append(fill_nan)
            else:
                imp_vect.append(prob_week_day_importance_vector[input_date.weekday()])

        day_importance[v_n] = imp_vect
    return day_importance


def compute_original_turnover_matrix(pred_turnover,input_year,input_month,num_of_meet_month):
    num_days = monthrange(int(input_year), int(input_month))[1]
    day_importance = {}
    turnover_venue = {}
    venue_name = pred_turnover.index
    flag=0
    for i, v_n in enumerate(venue_name):
        imp_vect = (np.ones(num_days))
        day_importance[v_n] = imp_vect
        turnover_venue[v_n]=day_importance[v_n]*pred_turnover[v_n]
        if flag==0:
            turnover_row= day_importance[v_n] * pred_turnover[v_n]
            flag=1
        else:
            turn_over_temp=day_importance[v_n]*pred_turnover[v_n]
            turnover_row=np.hstack((turnover_row, turn_over_temp))

    turnover_matrix = np.matlib.repmat(turnover_row, int(num_of_meet_month), 1)
    return turnover_matrix



def compute_turnover_matrix(pred_turnover,day_importance,num_of_meet_month):
    flag=0
    turnover_venue = {}
    venue_name=pred_turnover.index
    for v_n in venue_name:
        turnover_venue[v_n]=np.array(day_importance[v_n])*pred_turnover[v_n]
        if flag==0:
            turnover_row= np.array(day_importance[v_n]) * pred_turnover[v_n]
            flag=1
        else:
            turn_over_temp=np.array(day_importance[v_n])*pred_turnover[v_n]
            turnover_row=np.hstack((turnover_row, turn_over_temp))
    turnover_matrix = np.matlib.repmat(turnover_row, int(num_of_meet_month), 1)
    return turnover_matrix



def sechedule_meeting_preference(meet_number_series,turnover_original, turnover_prefernece,num_of_meet_month, input_year,input_month,day_preference_req):
    num_days = monthrange(int(input_year), int(input_month))[1]
    meeting = ['m' + str(i) for i in range(1, int(num_of_meet_month + 1), 1)]
    venues = [v for v in meet_number_series.index]
    dates = [input_year + '-' + input_month + '-' + str(i) for i in range(1, num_days + 1, 1)]
    #cons_venue = create_cons_venue(meet_dist_filt_venue)
    cons_venue = create_cons_venue_day_preference(meet_number_series,day_preference_req, num_days)
    cons_date = create_cons_date(num_days, input_year, input_month, num_of_meet_month)

    MS = MeetingSchedular(meeting, venues, dates,turnover_original)
    # if (len(MS.MEETING) > sum(cons_date.values())):
    #     return []
    # else:
    fixture_info=MS.do_bf_meeting_assignment(cons_venue, cons_date,turnover_prefernece)
    return fixture_info



def change_day_importance(day_importance, day_preference_req):
    day_importance_keys=list(day_importance.keys())
    for req in day_preference_req:
        if req['venue'] in day_importance_keys:
            for day in req['days']:
                if req['req_type'] == 'block':
                    #day_importance[req['venue']][day - 1] = 0
                    try:
                        day_importance[req['venue']][day-1] = 0
                    except Exception:
                        return day_importance
                elif req['req_type'] == 'high_priority':
                    #day_importance[req['venue']][day-1] = 1
                    try:
                        day_importance[req['venue']][day - 1] = 1
                    except Exception:
                        return day_importance
                elif req['req_type'] == 'medium_priority':
                    # day_importance[req['venue']][day-1] = .5
                    try:
                        day_importance[req['venue']][day - 1] = .5
                    except Exception:
                        return day_importance
    return day_importance

def compute_fixture(input_year, input_month, meet_number_series, pred_turnover_series, day_preference_req):

    meet_number=sum(meet_number_series)
    print('Total Meeting:', meet_number)
    # Create default day importance vector by putting equal importance on each day of the month
    day_importance=create_day_importance_vector(input_year,input_month, pred_turnover_series.index)
    # Change default day importance vector by user preference
    if 'Null' in day_preference_req:
        changed_day_importance = day_importance
        day_preference_req=[]
    else:
        changed_day_importance = change_day_importance(day_importance, day_preference_req)
    turnover_original=compute_original_turnover_matrix(pred_turnover_series,input_year,input_month,meet_number)
    turnover_preference= compute_turnover_matrix(pred_turnover_series, changed_day_importance, meet_number)
    #create meeting fixture using preference
    fixture=sechedule_meeting_preference(meet_number_series, turnover_original, turnover_preference, meet_number,
                                         input_year, input_month,day_preference_req)

    return  fixture


def create_fixture_for_whole_year(total_meet_num,input_year,day_preference_req_all_year):
    # ########### Estimate distribution of meeting over the months###############
    meet_dist_month = distribute_meet_to_months_by_year(total_meet_num)
    # ###############Select venues for each combination of meetings ##################
    month_index=list(meet_dist_month.keys())
    fixture_whole_year=[]

    for month_num,m_ind in enumerate(month_index):

        if len(day_preference_req_all_year)==0:
            day_preference_req_all_year = set_day_pref_to_null()


        num_of_meet_month = meet_dist_month[m_ind]
        month_num_=month_num+1
        if month_num_<10:
            input_month='0'+str(month_num_)
        else:
            input_month=str(month_num_)

        meet_number_series, pred_turnover_series = distribute_meet_to_venue_by_month_meet_type_combined(input_year, input_month, num_of_meet_month)
        fixture = compute_fixture(input_year, input_month,meet_number_series,pred_turnover_series, day_preference_req_all_year[month_num])
        print('Individual_fixture_length:',len(fixture))
        fixture_whole_year.append(fixture)

    fixture_dataframe = convert_fixture_list_to_dataframe(fixture_whole_year)
    #fixture_dataframe.to_csv('./data/auto_generated_fixture_2017.csv')
    non_fixed_meeting_dataframe = process_auto_generated_fixture(auto_fixture_path)
    temp_non_fixed=non_fixed_meeting_dataframe['Venue'].values
    non_fixed_meeting_dataframe['Flag']=list(np.zeros(len(temp_non_fixed)))
    fixed_meeting_dateframe = process_fixed_fixture(fixed_fixture_path)
    #print('Timbo',len(fixed_meeting_dateframe))
    pred_turnover_fixed_meeting = forcast_turnover_for_fixed_meeting(fixed_meeting_dateframe)
    fixed_meeting_dateframe['Pred_Turnover']=pred_turnover_fixed_meeting
    temp_fixed = fixed_meeting_dateframe['Venue'].values
    fixed_meeting_dateframe['Flag'] = list(np.ones(len(temp_fixed)))
    fixed_non_fixed_meeting = pd.concat([fixed_meeting_dateframe, non_fixed_meeting_dataframe])


    return non_fixed_meeting_dataframe,fixed_non_fixed_meeting



def set_day_pref_to_null():
    day_preference_MDY=[]
    day_preference_MDN=[]
    day_preference_MNY=[]
    day_preference_MNN = []
    day_preference_CDY=[]
    day_preference_CDN=[]
    day_preference_CNY=[]
    day_preference_CNN = []
    day_preference_req_all = [day_preference_MDY, day_preference_MDN, day_preference_MNY, day_preference_MNN,
                              day_preference_CDY, day_preference_CDN, day_preference_CNY, day_preference_CNN]
    return day_preference_req_all


def find_day_spread_constrain_prev_calendar(year,month,number_of_meet):
    turnover_data = pd.read_csv(TURNOVER_DATA)
    turnover_data_no_nan = turnover_data.dropna()
    prev_year=str(int(year)-1)
    prev_year_month=prev_year+'-'+month
    meet_date_prev_year = pd.to_datetime(turnover_data_no_nan[turnover_data_no_nan['Meet_Date'].str.contains(prev_year_month)]['Meet_Date'])
    meet_date_current_year= meet_date_prev_year.apply(lambda x: x + pd.DateOffset(years=1))
    #meet_date_current_year = meet_date_prev_year.apply(date_adjustment)
    # print(meet_date_prev_year)
    # print(meet_date_current_year)
    meet_num_by_date=meet_date_current_year.value_counts()
    norm_meet_num_by_date=meet_num_by_date/meet_num_by_date.sum()
    meet_spread_per_day_current_year=distribute_num_using_prob_dist(norm_meet_num_by_date,number_of_meet)
    #print(meet_spread_per_day_current_year)
    return meet_spread_per_day_current_year










