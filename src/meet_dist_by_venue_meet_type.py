import pandas as pd
from sklearn.ensemble import ExtraTreesRegressor
from sklearn import linear_model
import numpy as np
from random import randint
from matplotlib import pyplot as plt
from src.rv_util import distribute_num_using_prob_dist
import pickle
from os.path import *

NUM_OF_TREE=10
# TURNOVER_DATA= '../data/rv_turnover_per_meeting.csv'
TURNOVER_DATA= './data/rv_turnover_per_meeting.csv'

def find_month(full_date):
    splited_date=full_date.split('-')
    return (splited_date[1])


def forcast_turnover_per_venue(model, dm_venue, dm_month, input_venue_name, input_month,meet_property):
    # input_venue_name = 'Werribee'
    # input_month = '01'
    # input_day_night = 'D'

    #######Create dictonary for mapping input into one-hot encoded vector #############
    venue_name = dm_venue.columns
    venue_index = np.zeros([len(venue_name), 1])[0]

    # venue_index = np.zeros(len(venue_name))
    venue_series = pd.Series(list(venue_index), index=venue_name)

    month_name = dm_month.columns
    month_index = np.zeros([len(month_name), 1])[0]
    # month_index = np.zeros(len(month_name))
    month_series = pd.Series(list(month_index), index=month_name)
    #######encode input into one hot vector#############################################
    venue_series[input_venue_name] = 1
    month_series[input_month] = 1

    ###meet property####################################################################

    feature_meet_property=[]
    if meet_property['Meet_Type']=='Country':
        meet_type_flag=0
    else:
        meet_type_flag=1

    if meet_property['Day_Night']=='D':
        dn_flag=1
    else:
        dn_flag=0

    if meet_property['Tab_Status']=='Y':
        tb_flag=1
    else:
        tb_flag=0

    feature_meet_property.append(meet_type_flag)
    feature_meet_property.append(dn_flag)
    feature_meet_property.append(tb_flag)
    # feature_meet_property=np.reshape(np.asarray(feature_meet_property),[len(feature_meet_property),1])
    feature_meet_property=np.asarray(feature_meet_property)

    feature_venue = venue_series.values
    feature_month = month_series.values

    # print(feature_month)
    comb_feature = np.concatenate((feature_venue, feature_month,feature_meet_property))
    comb_feature = np.reshape(comb_feature, [len(comb_feature), 1])
    comb_feature = np.transpose(comb_feature)
    pred_turnover = np.exp(model.predict(comb_feature))[0]
    # print(input_venue_name,pred_turnover)

    return pred_turnover


def find_unique_venue_all_time(turnover_data_no_nan, meet_property):

    venue_list=turnover_data_no_nan[(turnover_data_no_nan['Meet_Type']==meet_property['Meet_Type']) \
                                    & (turnover_data_no_nan['Day_Night']==meet_property['Day_Night']) \
                                    & (turnover_data_no_nan['Tab_Status']==meet_property['Tab_Status'])]['Venue']
    unique_venue=set()
    for v in venue_list:
        unique_venue.add(v)
    unique_venue=sorted(unique_venue)
    return list(unique_venue)


def train_regression_model_month(turnover_data_no_nan):
    dm_venue = pd.get_dummies(turnover_data_no_nan['Venue'])
    df_turnover_data = turnover_data_no_nan['Total_Turnover']
    meet_date = turnover_data_no_nan['Meet_Date'].values
    meet_month = pd.Series((map(find_month, meet_date)))
    dm_month = pd.get_dummies(meet_month)
    dm_venue_ = dm_venue.values
    dm_month_ = dm_month.values
    turnover_data_no_nan['Meet_Type_Code'] = np.where(turnover_data_no_nan['Meet_Type'] == 'Country', 0, 1)
    turnover_data_no_nan['Day_Night_Code'] = np.where(turnover_data_no_nan['Day_Night'] == 'D', 1, 0)
    turnover_data_no_nan['Tab_Status_Code'] = np.where(turnover_data_no_nan['Tab_Status'] == 'Y', 1, 0)
    #print(turnover_data_no_nan.head(5))
    turnover_ = np.transpose(df_turnover_data.values)
    turnover_ = np.reshape(turnover_, [len(turnover_), 1])
    meet_type_=np.asarray(turnover_data_no_nan['Meet_Type_Code'].values)
    day_night_=np.asarray(turnover_data_no_nan['Day_Night_Code'].values)
    tab_status_=np.asarray(turnover_data_no_nan['Tab_Status_Code'].values)
    meet_type_=np.reshape(meet_type_,[len(meet_type_),1])
    day_night_=np.reshape(day_night_,[len(day_night_),1])
    tab_status_=np.reshape(tab_status_,[len(tab_status_),1])

    comb_venue_day_turnover = np.concatenate((dm_venue_, dm_month_,meet_type_,day_night_,tab_status_,turnover_), axis=1)
    # print(np.shape(comb_venue_day_turnover))
    x = comb_venue_day_turnover[:, 0:-1]
    y = comb_venue_day_turnover[:, -1]
    log_y = np.log(y + 1)
    model = linear_model.LinearRegression()
    #model = ExtraTreesRegressor(n_estimators=200,max_depth=10,random_state=0)
    model = model.fit(x, log_y)
    ##test prediction###
    # log_y_predicted=model.predict(x)
    # rmse=sqrt(mean_squared_error(log_y,log_y_predicted))
    # print('Turnover Perediction Error: ',rmse)

    return model,dm_venue,dm_month


def find_unq_venue_prev_calendar(year,month,turnover_data_no_nan,meet_property):
    prev_year=str(int(year)-1)
    prev_year_month=prev_year+'-'+month
    turnover_data_no_nan_filt_meet_prperty = turnover_data_no_nan[(turnover_data_no_nan['Meet_Type'] == meet_property['Meet_Type']) \
                                      & (turnover_data_no_nan['Day_Night'] == meet_property['Day_Night']) \
                                      & (turnover_data_no_nan['Tab_Status'] == meet_property['Tab_Status'])]

    # print(prev_year_month)
    temp_venue_filt_meet_property = list(turnover_data_no_nan_filt_meet_prperty[turnover_data_no_nan_filt_meet_prperty['Meet_Date'].str.contains(prev_year_month)]['Venue'])
    unq_venue_filt_meet_property_prev_year = set()
    for v in temp_venue_filt_meet_property:
        unq_venue_filt_meet_property_prev_year.add(v)

    unq_venue_filt_meet_property_prev_year = list(unq_venue_filt_meet_property_prev_year)

    venue_list = list(turnover_data_no_nan[turnover_data_no_nan['Meet_Date'].str.contains(prev_year_month)]['Venue'])
    unique_venue_all_prev_year = set()
    for v in venue_list:
        unique_venue_all_prev_year.add(v)

    unique_venue_all_prev_year = list(unique_venue_all_prev_year)

    return unique_venue_all_prev_year,unq_venue_filt_meet_property_prev_year


def distribute_meet_to_venue_by_month(input_year, input_month, num_of_meet_month,meet_property):
    turnover_data = pd.read_csv(TURNOVER_DATA)
    turnover_data_no_nan = turnover_data.dropna()
    #### Train regression model ################################################

    #save model and data
    if isfile('./model/model.pkl'):
        filename = './model/model.pkl'
        model=pickle.load(open(filename, 'rb'))
        filename = './model/dm_venue.pkl'
        dm_venue=pickle.load(open(filename, 'rb'))
        filename = './model/dm_month.pkl'
        dm_month=pickle.load(open(filename, 'rb'))


    else:
        model, dm_venue, dm_month = train_regression_model_month(turnover_data_no_nan)
        filename = './model/model.pkl'
        pickle.dump(model, open(filename, 'wb'))
        filename = './model/dm_venue.pkl'
        pickle.dump(dm_venue, open(filename, 'wb'))
        filename = './model/dm_month.pkl'
        pickle.dump(dm_month, open(filename, 'wb'))

        np.savez('./model/dummy_venue_month.npz', venue=dm_venue, month=dm_month)


    #### Forecast turnover by month for N vanues #############################################
    unq_venue_filt_meet_property_all_time = find_unique_venue_all_time(turnover_data_no_nan, meet_property)
    unique_venue_all_prev_year, unq_venue_filt_meet_property_prev_year=find_unq_venue_prev_calendar(input_year, input_month, turnover_data_no_nan, meet_property)
    # print('Num of All Unique venue filt by given meet property all time', len(unq_venue_filt_meet_property_all_time))
    # print('Num of All Unique venue last year', len(unique_venue_all_prev_year))
    # print('Num of All Unique venue filt by given meet property last year', len(unq_venue_filt_meet_property_prev_year))

    pred_turnover_list = []
    for input_venue_name in unq_venue_filt_meet_property_all_time:
        pred_turnover = forcast_turnover_per_venue(model, dm_venue, dm_month,
                                                   input_venue_name, input_month,meet_property)
        pred_turnover_list.append(pred_turnover)

    pred_turnover_series = pd.Series(pred_turnover_list, index=unq_venue_filt_meet_property_all_time)
    pred_turnover_series_sorted = pred_turnover_series.sort_values(ascending=False)
    ###Find unique number of meeting for a month from last input_year calendar##########
    percentage_meet_type=len(unq_venue_filt_meet_property_prev_year)/len(unique_venue_all_prev_year)
    num_meet=int((num_of_meet_month*percentage_meet_type))
    #### filter pred turnover series
    filt_pred_turnover = pred_turnover_series_sorted[:int(num_meet)]

    ########visulization of forecasted turnover #######################################
    # filt_pred_turnover.plot.bar()
    # plt.show()
    filt_turnover_sum = filt_pred_turnover.sum()
    norm_filt_pred_turnover = filt_pred_turnover / filt_turnover_sum

    meet_dist_filt_venue = distribute_num_using_prob_dist(norm_filt_pred_turnover, num_meet)

    ########visulization of constrain on the number of meet per venue #######################################
    # meet_dist_filt_venue.plot.bar()
    # plt.show()
    #print(len(meet_dist_filt_venue),len(filt_pred_turnover))

    return  meet_dist_filt_venue,filt_pred_turnover


def distribute_meet_to_venue_by_month_meet_type(input_year,input_month,num_of_meet_month):
    meet_type = ['Metro', 'Country']
    day_night = ['D', 'N']
    tab_status = ['Y', 'N']
    total_meet = []
    meet_dist_filt_venue_all = []
    filt_pred_turnover_all = []
    meet_property_all=[]
    for mt in meet_type:
        for dn in day_night:
            for ts in tab_status:
                meet_property = {'Meet_Type': mt, 'Day_Night': dn, 'Tab_Status': ts}
                meet_property_all.append(meet_property)
                # print(meet_property)
                meet_dist_filt_venue, filt_pred_turnover = distribute_meet_to_venue_by_month(input_year, input_month,
                                                                                             num_of_meet_month,
                                                                                             meet_property)
                total_meet.append(meet_dist_filt_venue.sum())
                print(total_meet)
                meet_dist_filt_venue_all.append(meet_dist_filt_venue)
                filt_pred_turnover_all.append(filt_pred_turnover)

    total_fixtured_meeting = sum(total_meet)
    extra_meeting = total_fixtured_meeting - num_of_meet_month

    if extra_meeting > 0:
        ind = total_meet.index(max(total_meet))
        meet_dist_filt_venue = meet_dist_filt_venue_all[ind]
        #venue = meet_dist_filt_venue.index
        meet_dist_filt_venue_sorted_des=meet_dist_filt_venue.sort_values(ascending=False)
        venue = meet_dist_filt_venue_sorted_des.index
        for v in venue:
            meet_dist_filt_venue_sorted_des[v] = meet_dist_filt_venue_sorted_des[v] - 1
            extra_meeting -= 1
            if extra_meeting == 0:
                break
        meet_dist_filt_venue_all[ind] = meet_dist_filt_venue_sorted_des

    if extra_meeting < 0:
        ind = total_meet.index(max(total_meet))
        meet_dist_filt_venue = meet_dist_filt_venue_all[ind]
        #venue = meet_dist_filt_venue.index
        meet_dist_filt_venue_sorted_asc = meet_dist_filt_venue.sort_values()
        venue = meet_dist_filt_venue_sorted_asc.index
        for v in venue:
            meet_dist_filt_venue_sorted_asc[v] = meet_dist_filt_venue_sorted_asc[v] + 1
            extra_meeting += 1
            if extra_meeting == 0:
                break
        meet_dist_filt_venue_all[ind] = meet_dist_filt_venue_sorted_asc

    # print('Number of Meeting after Adjusting',)
    # summ=[]
    # for m in meet_dist_filt_venue_all:
    #     summ.append(m.sum())
    # print(sum(summ))

    return meet_property_all,meet_dist_filt_venue_all,filt_pred_turnover_all


def map_meet_property_with_meet_distribution(meet_property_all,meet_dist_filt_venue_all,filt_pred_turnover_all):
    ind=0
    venue_meet_type=[]
    meet_number=[]
    pred_turnover=[]
    for meet_dist,turnover_dist in zip(meet_dist_filt_venue_all,filt_pred_turnover_all):
        if len(meet_dist)>0:
        # meet_dist=meet_dist[meet_dist>0]
            prefix='-'+meet_property_all[ind]['Meet_Type']+'-TS-'+meet_property_all[ind]['Tab_Status']+'-DN-'+meet_property_all[ind]['Day_Night']
            temp_venue_list=list(meet_dist.index)
            #temp_venue_list=list(map(lambda x:x.replace('-',""),temp_venue_list))
            temp_venue_meet_type_list=list(map((lambda x: x +prefix), temp_venue_list))
            venue_meet_type+=temp_venue_meet_type_list
            pred_turnover+=list(turnover_dist)
            meet_number+=list(meet_dist.values)

        ind+=1
    filt_venue_meet_type=[]
    filt_pred_turnover=[]
    filt_meet_number=[]
    for vm,pt,mn in zip(venue_meet_type,pred_turnover,meet_number):
        if mn>0:
            filt_venue_meet_type.append(vm)
            filt_pred_turnover.append(pt)
            filt_meet_number.append(mn)


    return filt_venue_meet_type,filt_pred_turnover,filt_meet_number



def distribute_meet_to_venue_by_month_meet_type_combined(input_year,input_month,num_of_meet_month):
    meet_property_all,meet_dist_filt_venue_all, filt_pred_turnover_all = distribute_meet_to_venue_by_month_meet_type(
        input_year, input_month, num_of_meet_month)

    filt_venue_meet_type, filt_pred_turnover, filt_meet_number = map_meet_property_with_meet_distribution(meet_property_all,
                                                                                meet_dist_filt_venue_all,filt_pred_turnover_all)

    meet_number_series=pd.Series(filt_meet_number,index=filt_venue_meet_type)
    pred_turnover_series=pd.Series(filt_pred_turnover,index=filt_venue_meet_type)

    return meet_number_series,pred_turnover_series





