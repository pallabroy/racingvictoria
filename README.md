# Demo code for creating fixture for the entire year  
    
    total_meet_non_fix = 420
    input_year = '2017'
    # block dates from fixed meeting list
    day_pref_input = create_fixed_block_preference()
    ## process day_prefence_input#################
    day_preference_req_all_year = process_day_prefence_input(day_pref_input)
    ## Create fixture for the whole year ##########
    non_fixed_meeting_dataframe,fixed_non_fixed_meeting=create_fixture_for_whole_year(total_meet_non_fix, input_year, day_preference_req_all_year)
    print('Number of Non Fixed Meeting',len(non_fixed_meeting_dataframe))
    print('Number of Total Meeting', len(fixed_non_fixed_meeting))
    print(fixed_non_fixed_meeting.to_csv('./optimized_fixture.csv'))


# Main steps of the automatic Fixture creator  algorithm 
- Estimate distribution of meetings over the months using historical data
- For each month:  
  * Select top *N* venues for *M* race meeting categories based on the forecasted turnover of the venues 
  * The number of meetings *(N)* for each race meeting category is estimated using the historical data 
  * Obtain a turnover matrix (row number: meet number, col number: number of days of given month * number of venues ) 
  * In the turonver matrix each raw represents a meeting and each column represents predicted turnover on that day for a venue (predicted turnover of v<sub>i</sub> * probability of the meeting occuracnce on the corresponding day d<sub>v<sub>i</sub></sub> ) 
  * Optimization algorithm will select a venue for each meeting that optimize the turnover using the turnover matrix  
  * Constrains are:
    - Maximum meeting per venue which is estimated from the percentage of the predicted turnover of the venue
    - Maximum number of meetings per day which is estimated from the historical data 
     
# Demo code for race programming   
    # List of races containg forcasted acceptance, wagering information and field size  per race 
    race_list=create_input_for_race_program_module()
    # Total capacity of the meeting 
    meet_capacity=180
    race_number,race_field_size=obtain_race_program(meet_capacity,race_list)
    print('Selected Race Field size Info')
    print(race_number)
    print('Selected Race Field size Info')
    print(race_field_size)
    
# Main steps of race programming algorithm 
- Sort descendingly all the races for a given meeting using forcasted wagering 
- For each race in the sorted race list: 
  * Compute number of this race using forcasted acceptance (num of total participants) and field size of this race (max number of horse per race)
  * Accumolate the perticants number per race
  * Terminate the loop when accumolated perticapants per race cross the max capacity of the meeting  

    
    
