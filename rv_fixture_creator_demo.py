from src.fixture_creator_utils import create_fixture_for_whole_year
from src.rv_util import process_day_prefence_input,create_fixed_block_preference

if __name__ == '__main__':
    # number of non fixed meeting
    total_meet_non_fix = 420
    input_year = '2017'
    # block dates from fixed meeting list
    day_pref_input = create_fixed_block_preference()
    ## process day_prefence_input#################
    day_preference_req_all_year = process_day_prefence_input(day_pref_input)
    ## Create fixture for the whole year ##########
    non_fixed_meeting_dataframe,fixed_non_fixed_meeting=create_fixture_for_whole_year(total_meet_non_fix, input_year, day_preference_req_all_year)
    print('Number of Non Fixed Meeting',len(non_fixed_meeting_dataframe))
    print('Number of Total Meeting', len(fixed_non_fixed_meeting))
    print(fixed_non_fixed_meeting.to_csv('./optimized_fixture.csv'))

